# :wave: Hello, I'm Justin

You've found my README! My name is Justin Farris and I'm the Sr Director of Product Monetization at GitLab. This README document will hopefully help you get to know me a bit better and understand what it's like working with me. 

## Personal

* I'm originally from [Spokane, WA](https://en.wikipedia.org/wiki/Spokane,_Washington) where I grew up and attended college
* I went to [Whitworth University](https://en.wikipedia.org/wiki/Whitworth_University) which was only 10mins from my childhood home. 
* I moved to Seattle, WA in 2011 and now live in Edmonds, WA which is ~20mins north of Downtown. 
* I live with my wife (Ingrid, Married since 2016) our two children (Milla & Carson) and Copper our Golden Retriever. There is a good chance you may meet one of them on a zoom call.

* Justin's Career Jungle Gym
  1. Internship at [NXNW](https://nxnw.net/) in Spoakane booking flights for actors flying into town to shoot a movie.
  1. [Various roles working on movies](https://www.imdb.com/name/nm3963821/?ref_=fn_al_nm_2). Notably a lighting tech on ["Knights of Badassdom"](https://www.imdb.com/title/tt1545660/?ref_=nm_knf_i3)
  1. "Genius" at the [Apple Store](https://www.apple.com/retail/riverparksquare/)
  1. Internship that turned into a full fime job at a small Augmented Reality startup in Spoakne (web devloper, designer)
  1. Product Manager & Director of Product at AR startup in Spokane
  1. Moved to Seattle, became PM for a small incubation startup
  1. Took a PM job at [Zillow](https://www.zillow.com/) as PM, Strategy in the CRO org. I learned a lot about sales process, efficency.
  1. Transfered internally to run Growth at Zillow Sr PM, GPM, Director of Product. Similar role to Product Monetization at GitLab. 
  1. Joined GitLab after ~6 yrs at Zillow as GPM, Plan Stage
  1. Transfered internally to GPM, Fulfillment. Promoted to Director
  1. Transfered to Sr Director, Product Monetization as of January 2022
* Outside of work I spend a lot of time with my family. Chasing small children is both rewarding and exausting. 
* I enjoy all things sci-fi and fantasy. If you have a recommendation please share it
* I play video games with friends. Espeically since COVID started it's been a great outlet to stay connected with friends. 
* We're in the midst of remodeling our house. I love real estate so being able to design what you want from sctach has been fun. Also I'm lousy at drywall work. 
* I've always loved lego, and have reignighted that passion in recent years. Feel free and ask me to zoom out my camera in our next call and I can show you more of the collection.

## General

* I've spent the majority of my career working on consumer products or marketplaces. GitLab is my first SaaS company so I'm still learning. 
* I love finding simple solutions to giant problems. If I ever over simplify something please call me out on it, nuance is important and the details matter.  
* I dislike getting stuck debating minutiae. It's hard to solve big problems when you're focused solving for small details. This is one of the reasons I love ouf Value of [Iteration](https://about.gitlab.com/handbook/values/#iteration). Just remember to always think big, and iterate small. 
* I very much prefer consuming content asyncronous. If you have an idea you want to present to me please summarize in a doc, presentation or record a video. If you'd like to discuss it syncronously let's use the time as a Q&A
* I'm very optimistic. I'm a firm beleiver that if you give people the right direction, tools, and autonomy they can achieve great things. Sometimes my optimism gets the best of me, keep me honest when I need to be more pessimistic. 
* I value relationships, and believe strongly in the power of healthy relationships. Especially when it comes to achieving results. Please schedule coffee chats with me, It's a priority for me each week and helps me feel connected to everyone I work with. 
* I may respond to things at weird hours. Chalk it up to having small children. I also prefer to respond asyncronosuly so it may take me some time to get back to you. I don't expect immediate responses from you, and you shoudln't expect them from me. Especially during "off" hours. 
* As a leader I feel it's really important not to meddle in the details of an area, especailly when you've hired an expert to be the DRI. Assume anything I say is a suggestion, not direction unless I state otherwise. If you ever find that I'm being unclear ask me to clarify. 
* I'm ambitious but can sometimes over commit. I'm working on improving here so please keep me accountable if I push too hard or need to say "no" more. 
* I have a strong bias for action and push team members to make decisions and commit to them. I may ask you to disagree and commit from time to time, challenge me if you feel like any decision is not a two way door. 


## Product & Product Monetization

* I love Growth and all things traditionally under a growth or Product Monetization function (Growth, Fullfillment, Pricing, etc). Some of my favorite content:
  * [Lenny Rachitsky's blog](https://www.lennyrachitsky.com/)
  * [Reforge](https://www.reforge.com/) (full disclosure: I'm a proud investor)
  * [Andrew Chen](https://www.amazon.com/Cold-Start-Problem-Andrew-Chen/dp/0062969749)

* I spend a fair amount of time studying business + tech strategy. Some of my favorite resorces:
  * [Stratechery](https://stratechery.com/)
  * [Dithering](https://daringfireball.net/2020/05/dithering)
  * [The Infromation](https://www.theinformation.com/)

* Spending time working on consumer or B-to-SMB products I've grown a strong appreciation for great user experiences. I may be opinionated but I'm not an expert (that's the job of our amazing UX team). Some of my favorite resources are [UI Teneants and Traps](https://uitraps.com/), [Experience Outcomes](https://medium.com/technology-hits/why-choose-ux-outcomes-over-business-outcome-f491dcbb0965), [JTBD](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done).

* Pricing is a relatively new domain for me, at least as it pertains to SaaS and pricing for larger enteprrises. I'm learning here (especially from our venerable Pricing team), but if you have any resources you like please share. Lately I've been enjoying watching the back catalog ofd ProfitWell's [Pricing Page Taredowns](https://www.youtube.com/playlist?list=PLdZW0nVWzzEQ_VIBu7OhNzl_4ulUlu_h0)

## Zoom / All Remote Setup
I often am asked about my setup (camera, audio, etc etc). My setup is a bit overkill, but pair down any of the components below and you should be able to achieve similar results. Here's a quick primer:

**Audio**
* [Sure SM7B](https://www.amazon.com/Shure-SM7B-Cardioid-Dynamic-Microphone/dp/B0002E4Z8M)
* [Cloud Lifter](https://www.amazon.com/Cloud-Microphones-CL-1-Cloudlifter-1-channel/dp/B004MQSV04/) (pre-amplifies signal from the SM7B)
* [Focusrite Scarlett](https://www.amazon.com/Focusrite-Scarlett-Audio-Interface-Tools/dp/B07QR6Z1JB/)
* [Mackie Studio Monitors](https://www.amazon.com/Mackie-Professional-Performance-Translation-Logarithmic/dp/B073WN6WQJ/) 

**Video**
* [Canon EOS-M 200](https://www.usa.canon.com/internet/portal/us/home/products/details/cameras/eos-dslr-and-mirrorless-cameras/mirrorless/eos-m200-ef-m-15-45mm-is-stm-kit)
* [Elgato CamLink 4k](https://www.amazon.com/Elgato-Cam-Link-Broadcast-Camcorder/dp/B07K3FN5MR/)
* [Mini HDMI to HDMI Cable](https://www.amazon.com/Synergy-Digital-Camera-Mirrorless-Definition/dp/B08D8NZTRN)

**Lights**
* [Elgato Key Light x2](https://www.elgato.com/en/key-light)

**Misc**
* [Elgato Stream Deck, controls lights & programmable](https://www.amazon.com/Elgato-Stream-Deck-MK-2-Controller/dp/B09738CV2G/)

